package steps;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
	
	public ChromeDriver driver;
		
	@Given("open chrome browser")
	public void openChromeBrowser() {
	    // Write code here that turns the phrase above into concrete actions
	    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
        driver = new ChromeDriver();
	}

	@And("Maximize the browser")
	public void maximizeTheBrowser() {
	   
		driver.manage().window().maximize();
		
	}

	@And("set the Timeout")
	public void setTheTimeout() {
	    
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
	}

	@And("Enter the Url")
	public void enterTheUrl() {
		
		driver.get("http://leaftaps.com/opentaps");
	    
	}

	@And("Enter the Username as (.*)")
	public void enterTheUsername(String uname) {
	    
		driver.findElementById("username").sendKeys(uname);
	}

	@And("Enter the password as (.*)")
	public void enterThePassword(String upass) {
		driver.findElementById("password").sendKeys(upass);
	    
	}

	@When("Click on Login Button")
	public void clickOnLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	   
	}

	@Then("Verify login is success")
	public void verifyLoginIsSuccess() {
	    System.out.println("VERIFIED SUCCESFULLY");
	}
			
	
	@When("Verify login is fail")
	public void verifyLoginIsFail() {
	   System.out.println("VERIFIED WRONG");
	}
	
	@When("Click on CRMSFAlink")
	public void  ClickonCRMSFAlink() {
		
		driver.findElementByPartialLinkText("CRM/SFA").click();
		
	}
	
	@When("click on CreateLeadLink")
	public void clickCreateLead() {
		
		driver.findElementByLinkText("Create Lead").click();
		
		
	}

	@And("Enter companyname as (.*)")	 
 public void enterCompanyName(String cname) {
		
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	 
	 
 }
	
	
	@And("Enter Firstname as (.*)")	 
	 public void EnterFirstname(String Fname) {
			
			driver.findElementById("createLeadForm_firstName").sendKeys(Fname);
		 
		 
	 }
	
	
	@And("Enter Lastname as (.*)")	 
	 public void EnterLaststname(String Lname) {
			
			driver.findElementById("createLeadForm_lastName").sendKeys(Lname);
		 
		 
	 }
	
	@When("clickSubmitButton")
	public void clickSubmit() {
		
		driver.findElementByName("submitButton").click();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		

}
