package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;



@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/featureFile/Login.feature",
 				  glue="steps",
 				  monochrome=true,
 				  dryRun = false,
 				  snippets=SnippetType.CAMELCASE,
 				 plugin= {"pretty",
 		              	  "html:reports"
 		              	+ "/cucumberBasicReport"})
public class Runtest {
	
	
	

}
