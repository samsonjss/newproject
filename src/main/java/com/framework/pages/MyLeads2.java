package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeads2 extends ProjectMethods{

	public MyLeads2() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	
	@FindBy(how = How.LINK_TEXT,using="Create Lead") WebElement elelink;
	@FindBy(how = How.LINK_TEXT,using="Create Lead") WebElement elelink1;
	@FindBy(how = How.LINK_TEXT,using="Create Lead") WebElement elelink2;

	public CreateLead clickCreateLead() {
		
		click( elelink);
		
		return new  CreateLead();
		
		
	}
	
public CreateLead clickFindLead() {
		
		click( elelink1);
		
		return new  CreateLead();
		
		
	}

public CreateLead clickMergeLead() {
	
	click( elelink2);
	
	return new  CreateLead();
	
	
}
	
	
	
	
	
}













