Feature: Login for Leaftaps

Background:
Given open chrome browser

And Maximize the browser

And set the Timeout

And Enter the Url


Scenario Outline: positive flow

And Enter the Username as <uname> 

And Enter the password as <upass>

When  Click on Login Button

Then Verify login is success

When Click on CRMSFAlink

When click on CreateLeadLink

And Enter companyname as SAMSON

And Enter Firstname as SAM

And Enter Lastname as SINGH

When clickSubmitButton

Examples:
   
   |uname|upass|
   |DemoSalesManager|crmsfa|
   |DemoSalesManager1|crmsfa1|
   

Scenario: NEGATIVE flow

And Enter the Username as DemoCSR 

And Enter the password as crmsfa

When  Click on Login Button

But Verify login is fail



